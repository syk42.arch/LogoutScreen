#include "include/LS.hpp"
#include <QScreen>
#include <QDesktopWidget>
#include <QVector>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  QRect screenRec = QGuiApplication::screens()[0]->geometry();
  LS dia;
  dia.setParent(0);
  dia.setMinimumSize(screenRec.width(), screenRec.height());
  dia.setMaximumSize(screenRec.width(), screenRec.height());
  dia.move(QPoint(screenRec.x(), screenRec.y()));
  dia.activateWindow();
  dia.raise();
  dia.setFocus();
  dia.setVisible(true);

  LS dia1(&dia);
  QVector<LS> dias(QGuiApplication::screens().size()-2, dia1);

  if (QGuiApplication::screens().size() >= 2) {

    screenRec = QGuiApplication::screens()[1]->geometry();
    dia1.setMinimumSize(screenRec.width(), screenRec.height());
    dia1.setMaximumSize(screenRec.width(), screenRec.height());
    dia1.move(QPoint(screenRec.x(), screenRec.y()));
    dia1.activateWindow();
    dia1.raise();
    dia1.setVisible(true);
    dia1.open();

    for (int i = 2; i < QGuiApplication::screens().size(); i++) {
      screenRec = QGuiApplication::screens()[i]->geometry();
      dias[i-1].setMinimumSize(screenRec.width(), screenRec.height());
      dias[i-1].setMaximumSize(screenRec.width(), screenRec.height());
      dias[i-1].move(QPoint(screenRec.x(), screenRec.y()));
      dias[i-1].activateWindow();
      dias[i-1].raise();
      dias[i-1].setVisible(true);
    }
  }
  return app.exec();
}
