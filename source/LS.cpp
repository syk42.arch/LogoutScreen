#include "include/LS.hpp"
#include <QtWidgets>
#include <QApplication>
#include <stdlib.h>
#include <iostream>

LS::LS(QWidget *parent) : QDialog(parent), ui(new Ui::ls_window) {

  setWindowFlags(Qt::Popup | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint);
  //setAttribute(Qt::WA_NoSystemBackground, true);
  //setAttribute(Qt::WA_TranslucentBackground, true);

  ui->setupUi(this);
  this->setWindowTitle("Logout");
}

LS::LS(const LS& to_copy) : QDialog(to_copy.parentWidget()), ui(new Ui::ls_window) {
  setWindowFlags(Qt::Popup | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint);
  //setAttribute(Qt::WA_NoSystemBackground, true);
  //setAttribute(Qt::WA_TranslucentBackground, true);

  ui->setupUi(this);
  this->setWindowTitle("Logout");
}

LS::~LS() {
  delete ui;
}

void LS::on_Cancel_pressed() {
  this->setVisible(false);
  QCoreApplication::exit();
}

void LS::on_ShutDown_pressed() {
  system("shutdown 0");
  QCoreApplication::exit();
}

void LS::on_Reboot_pressed() {
  system("reboot");
  QCoreApplication::exit();
}

void LS::on_Logout_pressed() {
  std::string session = this->get_session_id();
  system(("loginctl terminate-session " + session).c_str());
  QCoreApplication::exit();
}

void LS::on_Lock_pressed() {
  std::string session = this->get_session_id();
  system(("loginctl lock-session " + session).c_str());
  QCoreApplication::exit();
}

std::string LS::get_session_id() {
  std::string session_id = "";
  session_id += std::getenv("XDG_SESSION_ID");
  return session_id;
}

void LS::keyPressEvent(QKeyEvent *e) {

  std::string session = this->get_session_id();

  switch (e->key()) {
        case Qt::Key_Escape:
          break;
        case Qt::Key_R:
          system("reboot");
          break;
        case Qt::Key_S:
          system("shutdown 0");
          break;
        case Qt::Key_L:
          system(("loginctl terminate-session " + session).c_str());
          break;
        case Qt::Key_X:
          this->clearFocus();
          this->close();
          system(("loginctl lock-session " + session).c_str());
      break;
    }
  QCoreApplication::exit();
}
