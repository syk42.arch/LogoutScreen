#pragma once

#include <QtWidgets/QDialog>
#include "ui_ls_window.h"
#include <string>

class LS : public QDialog {

  Q_OBJECT;

public:
  explicit LS(QWidget *parent = nullptr);

  LS(const LS&);

  virtual ~LS();

private slots:

  void on_Cancel_pressed();

  void on_ShutDown_pressed();

  void on_Reboot_pressed();

  void on_Logout_pressed();

  void on_Lock_pressed();

private:
  Ui::ls_window *ui;

  std::string get_session_id();

protected:
  void keyPressEvent(QKeyEvent *e);

};
