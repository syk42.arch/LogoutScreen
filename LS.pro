
SRC_DIR = source
INC_DIR = source/include
FORM_DIR = source/form

QT += widgets

FORMS = $$FORM_DIR/ls_window.ui

SOURCES += $$SRC_DIR/main.cpp
SOURCES += $$SRC_DIR/LS.cpp

HEADERS += $$INC_DIR/LS.hpp

TARGET = logout_screen

target.path += $${PREFIX}/bin
INSTALLS += target
